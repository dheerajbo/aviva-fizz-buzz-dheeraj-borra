package com.program.fizzbuzz;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import junit.framework.Assert;

/**
 * @author Dheeraj Borra
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FizzbuzzApplicationTests {

	/**
	 * This method will do the JUNIT testing and process the expected output from
	 * the program
	 */
	@Test
	public void test_fizzBuzzValidation() {

		FizzBuzzController buzzController = new FizzBuzzController();

		Assert.assertEquals("Please enter valid POSITIVE number between 1 to 1000",
				buzzController.FizzBuzzValidation(-1));

		Assert.assertEquals(
				"1</br>2</br>fizz</br>4</br>buzz</br>fizz</br>7</br>8</br>fizz</br>buzz</br>11</br>fizz</br>13</br>14</br>",
				buzzController.FizzBuzzValidation(15));

		Assert.assertEquals(
				"1</br>2</br>fizz</br>4</br>buzz</br>fizz</br>7</br>8</br>fizz</br>buzz</br>11</br>fizz</br>13</br>14</br>fizzbuzz</br>16</br>17</br>fizz</br>19</br>",
				buzzController.FizzBuzzValidation(20));

		Assert.assertEquals("Please enter valid POSITIVE number between 1 to 1000",
				buzzController.FizzBuzzValidation(1001));

	}

}
