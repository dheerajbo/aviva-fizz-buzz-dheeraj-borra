package com.program.fizzbuzz;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import junit.framework.Assert;

/**
 * @author Dheeraj Borra
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FizzBuzzApplicationPaginationTests {

	/**
	 * This method will do the JUNIT testing and process the expected output from
	 * the program
	 */
	@Test
	public void test_fizzbuzzPagination() {

		FizzbuzzPagination buzzController = new FizzbuzzPagination();

		Assert.assertEquals(
				"61</br>62</br>fizz</br>64</br>buzz</br>fizz</br>67</br>68</br>fizz</br>buzz</br>71</br>fizz</br>73</br>74</br>fizzbuzz</br>76</br>77</br>fizz</br>79</br>buzz</br>",
				buzzController.FizzBuzzValidation(3, 5));

		Assert.assertEquals(
				"101</br>fizz</br>103</br>104</br>fizzbuzz</br>106</br>107</br>fizz</br>109</br>buzz</br>fizz</br>112</br>113</br>fizz</br>buzz</br>116</br>fizz</br>118</br>119</br>fizzbuzz</br>",
				buzzController.FizzBuzzValidation(5, 7));

	}

}
