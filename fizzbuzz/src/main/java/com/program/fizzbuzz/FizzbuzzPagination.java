package com.program.fizzbuzz;

import java.util.Calendar;

import javax.validation.ValidationException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Dheeraj Borra
 *
 */
@RestController
public class FizzbuzzPagination {

	String fizz = "fizz";
	String buzz = "buzz";
	String fizzbuss = "fizzbuzz";

	/**
	 * This method will take two input values
	 * 
	 * @param prev
	 * @param next
	 * @return String
	 * @throws ValidationException
	 */
	@RequestMapping("/fizzbuzzPagination")
	public String FizzBuzzValidation(@RequestParam("prev") int prev, @RequestParam("next") int next)
			throws ValidationException {

		fizz = "fizz";
		buzz = "buzz";
		fizzbuss = "fizzbuzz";

		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);

		if (day == 3) {
			fizz = "wizz";
			buzz = "wuzz";
		}

		StringBuffer s1 = splittingString(prev, next);

		return s1.toString();
	}

	/**
	 * This method will take two input values
	 * 
	 * @param prev
	 * @param next
	 * @return StringBuffer
	 */
	public StringBuffer splittingString(int prev, int next) {

		StringBuffer buffer = new StringBuffer();
		for (int i = (prev * 20) + 1; i <= (next - 1) * 20; i++) {

			boolean isFlag = false;
			if (i % 3 == 0 && i % 5 == 0) {
				isFlag = true;
				buffer.append(fizzbuss);
			}
			if (i % 3 == 0 && !isFlag) {
				isFlag = true;
				buffer.append(fizz);
			}
			if (i % 5 == 0 && !isFlag) {
				isFlag = true;
				buffer.append(buzz);
			} else if (!isFlag) {
				buffer.append(i);
			}
			buffer.append("</br>");
		}
		return buffer;
	}

	/**
	 * This method will take the String and form the output of String
	 * 
	 * @param integer
	 *            value n
	 * @return String
	 */
	public String formingString(int n) {

		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < n; i++) {
			stringBuffer.append(i + "</br>");
		}
		return stringBuffer.toString();
	}

}
