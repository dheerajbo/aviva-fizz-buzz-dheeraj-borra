package com.program.fizzbuzz;

import java.util.Calendar;

import javax.validation.ValidationException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Dheeraj Borra
 *
 */
@RestController
public class FizzBuzzController {

	String fizz = "fizz";
	String buzz = "buzz";
	String fizzbuss = "fizzbuzz";

	/**
	 * This method will take value as input
	 * 
	 * @param value
	 * @return string as a output
	 * @throws ValidationException
	 */
	@RequestMapping("/fizzbuzzValidation")
	public String FizzBuzzValidation(@RequestParam("value") int value) throws ValidationException {
		fizz = "fizz";
		buzz = "buzz";
		fizzbuss = "fizzbuzz";

		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);

		if (day == 3) {
			fizz = "wizz";
			buzz = "wuzz";
		}

		StringBuffer stringBuffer = new StringBuffer();

		if ((value >= 1 && value < 1000)) {
			return processInput(value, stringBuffer);
		} else {
			stringBuffer.append("Please enter valid POSITIVE number between 1 to 1000");
		}
		return stringBuffer.toString();
	}

	/**
	 * This method will process input and add it to stringbuffer
	 * 
	 * @param value
	 * @param buffer
	 * @return String
	 */
	private String processInput(int value, StringBuffer buffer) {

		for (int i = 1; i < value; i++) {
			boolean isFlag = false;

			if (i % 3 == 0 && i % 5 == 0) {
				isFlag = true;
				buffer.append(fizzbuss);
			}

			if (i % 3 == 0 && !isFlag) {
				isFlag = true;
				buffer.append(fizz);
			}

			if (i % 5 == 0 && !isFlag) {
				isFlag = true;
				buffer.append(buzz);
			} else if (!isFlag) {
				buffer.append(i);
			}
			buffer.append("</br>");
		}
		return buffer.toString();
	}
}
